# Minigame Game
Installation:

You need minetest running as a server.

Releases can be downloaded from here:

https://gitlab.com/mistere123.coding/minigame_game/-/jobs
For each commit, a new zip file is created as an artifact. Download it, extract it twice, and place the minigame_game folder in the games folder of Minetest.

or by cloning the repo:

`git clone --recursive https://gitlab.com/mistere123.coding/minigame_game`

Updating with git:

`git pull`
`git submodule update`




This game provides all the mods you need to set up a minigame server. 

# Game Setup:

1) Start a new world with minigame_game

2) Find a place to make your minigame hub; note your spawn position. 

3) Enter the world folder and find <world>/hub/SETTINGS.lua 

Edit:
```lua
-- spawn point
hub.settings.hub_spawn_point = vector.new(5, 10, 5)
```

Note the other Hub settings in the file: Hub BGM, Hotbar items, physics
overrides, etc. Edit those at will.

Place HUB Background Music in the BGM folder.

# Arena setup:

Minigame_game is a collection of everything you need to run a minigame server, except arenas.

For each game you want to set up, you will need an arena (except for a few minigames which create their own arenas: even those, you should set aside a location in the world for them by following the steps):

To set up a minigame arena:

First, build (or import) an arena for the minigame. Each minigame may have special requirements for what the arena must be:
Most will need to be entirely enclosed to prevent players from escaping the match.
Some, like block_league, will need goals and other special map locations. Block league in particular requires a ball spawn area and then for each team: a spawn area, a goal area, and a jail area.
Wormball requires a fully enclosed, visible, 3d space to move around in, which should be cubic but can have obstacles inside. 
Other minigames's requirements can be found by looking at their folders' readme or looking at examples or by inspecting the properties one must set.

To list all the minigames available, 

`/arenas gamelist`

to create an arena:
`/arenas create <game_name> <arena_name>`

to edit it,
`/arenas edit <game_name> <arena_name>`

That will bring you to an editor where you can set all the properties.

Hint, if there is a special editor tool for the minigame, you probably don't have to change the arena properties manually.
If there is not a special editor tool, then use the gear tool section and the properties tool to set the specific arena properties.

You will usually need to set the arena region with the map tool. You may need to save the map for resetting later using the provided tool.

I will defer to arena_lib documentation for other details, but that should get you started.

