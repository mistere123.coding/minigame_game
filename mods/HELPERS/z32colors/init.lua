z32 = {}
-- defined as a table, z32.colors,
z32.colors = {
    "#472d3c",
    "#5e3643",
    "#7a444a",
    "#a05b53",
    "#bf7958",
    "#eea160",
    "#f4cca1",
    "#b6d53c",
    "#71aa34",
    "#397b44",
    "#3c5956",
    "#302c2e",
    "#5a5353",
    "#7d7071",
    "#a0938e",
    "#cfc6b8",
    "#dff6f5",
    "#8aebf1",
    "#28ccdf",
    "#3978a8",
    "#394778",
    "#39314b",
    "#564064",
    "#8e478c",
    "#cd6093",
    "#ffaeb6",
    "#f4b41b",
    "#f47e1b",
    "#e6482e",
    "#a93b3b",
    "#827094",
    "#4f546b"
}

-- register a cracky minetest node for each color by generating a single pixel texture with the color
for i, color in ipairs(z32.colors) do
    minetest.register_node("z32colors:color_"..i, {
        description = "Color "..i,
        -- use minetest texture modifiers to generate a single-pixel texture using the color from the z32.colors table
        tiles = {"z32colors_color.png^[colorize:"..color},
        groups = {cracky=3},
        -- use default node sounds for stone, but reduce the volume
        sounds = default.node_sound_stone_defaults(),
    })
    stairsplus:register_all("z32colors", "color_"..i, "z32colors:color_"..i, {
        description = "Color "..i,
        tiles = {"z32colors_color.png^[colorize:"..color},
        groups = {cracky=3},
        sounds = default.node_sound_stone_defaults(),
    })
end

