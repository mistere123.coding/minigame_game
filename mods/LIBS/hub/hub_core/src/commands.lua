local S = minetest.get_translator("hub")

local al_blocked_cmds = table.key_value_swap(arena_lib.BLOCKED_CMDS)
local blocked_cmds = table.key_value_swap(hub.settings.blocked_cmds)
local blocked_cmds_both = {}

for k, v in pairs(blocked_cmds) do
    if al_blocked_cmds[k] then
        blocked_cmds_both[k] = true
    end
end



minetest.register_on_chatcommand(function(name, command, params)
    if not minetest.check_player_privs(name, "hub_admin") then
        if blocked_cmds_both[command] then
            hub.warn_player(name, S("Admins have blocked this command"))
            return true
        end

        if blocked_cmds[command] and not arena_lib.is_player_in_arena(name) then
            hub.warn_player(name, S("Admins have blocked this command whilst in the lobby"))
            return true
        end
    end

    if al_blocked_cmds[command] and arena_lib.is_player_in_arena(name) then
        arena_lib.print_error(name, S("There's a time and place for everything. But not now!"))
        return true
    end
end)



ChatCmdBuilder.new("hub", function(cmd)
    cmd:sub("celvault", function(sender)
        hub.edit_celvault(sender)
    end)

    cmd:sub("chat off", function(sender)
        minetest.chat_send_player(sender, "Hub chat is now off")
        hub.chat = false
    end)

    cmd:sub("chat on", function(sender)
        minetest.chat_send_player(sender, "Hub chat is now on")
        hub.chat = true
    end)

end, {
    description = [[
        - celvault
        - chat off/on
        ]],
    privs = { hub_admin = true }
})