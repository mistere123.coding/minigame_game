local function trim_name() end

local arenas = {}                       -- KEY: sorting ID, VALUE: {mod = mod, mg = mg_name, name = a_name, icon = mg_icon, y_off, bg = txt.png, amount = txt}
local hidden_panels = {}                -- KEY: p_name, VALUE: true
local MAX_ARENAS_IN_STATUS = hub.settings.MAX_ARENAS_IN_STATUS
local SLOT_DISTANCE = 52

-- I pannelli si dividono nelle tabelle contenenti le informazioni da mostrare (`arenas`)
-- e nella loro resa grafica vera e propria (panel_lib). Ogni volta che un pannello
-- viene aggiornato, pesca i dati da `arenas`.
-- I pannelli sono gestiti in modo tale che le loro posizioni non si intersechino mai:
-- il pannello più in alto sarà sempre il n°1, quello seguente il n°2 ecc. Se l'arena
-- associata al primo pannello scompare, le informazioni del pannello n°2 passeranno
-- a quest'ultimo e via dicendo, nascondendo quello visibile più in fondo (a meno che
-- il numero di `arenas` non superi il numero di pannelli visibili: in quel caso
-- otterrà le informazioni dal primo non visibile)

function hub.HUD_arstatus_create(p_name)
  for i = 1, MAX_ARENAS_IN_STATUS do

    local a_data  = arenas[i]
    local y_off   = a_data and a_data.y_off or 0
    local bg      = a_data and a_data.bg or "blank.png"
    local icon    = a_data and a_data.icon or "blank.png"
    local a_name  = a_data and trim_name(a_data.name) or ""
    local amount  = a_data and a_data.amount or ""

    Panel:new("hubman_arenastatus_" .. i, {
      player = p_name,
      position  = {x = 1, y = 0.5},
      alignment = {x = -1, y = 0},
      offset = {x = 0, y = y_off},
      bg = bg,
      bg_scale = {x = 3, y = 3},
      title = "",
      sub_img_elems = {
        icon = {
          text = icon,
          offset = {x = -180}
        }
      },
      sub_txt_elems = {
        arena_name = {
          text = a_name,
          alignment = {x = 1},
          offset = {x = -175}
        },
        p_amount = {
          text = amount,
          alignment = {x = -1},
          offset = {x = -5}
        }
      }
    })
  end

  -- se si son sconnessi mentre avevano i pannelli nascosti e poi riconnessi,
  -- hidden_panels sarà ancora `true`. Reimposto per sicurezza
  hidden_panels[p_name] = nil
end



function hub.HUD_arstatus_add(mod, arena)
  local arenas_amnt = #arenas
  local y_off

  -- calcolo lo scostamento che avrà il pannello. Se sono già visualizzati i pannelli massimi,
  -- lo accoderò in basso di un'unità. Al contrario, se questo sarà visibile,
  -- sposterò tutti quelli visibili di mezza unità verso l'alto, accodando quello
  -- nuovo sempre di mezza unità
  if arenas_amnt == 0 then
    y_off = 0
  else
    if arenas_amnt >= MAX_ARENAS_IN_STATUS then
      y_off = arenas[arenas_amnt].y_off + SLOT_DISTANCE
    else
      y_off = arenas[arenas_amnt].y_off + SLOT_DISTANCE / 2

      for _, slot in ipairs(arenas) do
        slot.y_off = slot.y_off - SLOT_DISTANCE / 2
      end
    end
  end

  -- aggiungo l'arena nella tabella
  local bg_img = arena.players_amount < arena.max_players and "hub_arenastatus_join.png" or "hub_arenastatus_nojoin.png"
  local mg = arena_lib.mods[mod]
  local a_info = {
    mod     = mod,
    mg      = mg.name,
    name    = arena.name,
    icon    = mg.icon,
    y_off   = y_off,
    bg      = bg_img,
    amount  = arena.players_amount .. "/" .. arena.max_players * #arena.teams
  }

  table.insert(arenas, a_info)

  -- se supera il numero massimo di slot visibili, la grafica non verrebbe aggiornata ugualmente
  if #arenas > MAX_ARENAS_IN_STATUS then return end

  for i = 1, #arenas do
    hub.HUD_arstatus_update(i)
  end
end



function hub.HUD_arstatus_remove(mod, arena_name)
  -- trovo l'ID dell'arena nell'HUD
  local HUD_ID = hub.get_arenastatus_slot(mod, arena_name)

  table.remove(arenas, HUD_ID)

  local arenas_amnt = #arenas

  -- scalo la posizione dei pannelli che venivano dopo quello rimosso. Li muovo
  -- di mezza unità verso l'alto se quelli visibili cambieranno di numero, o di
  -- una intera se erano già al completo (ovvero uguali o maggiori di MAX_ARENAS_IN_STATUS)
  local mov_divisor = arenas_amnt < MAX_ARENAS_IN_STATUS and 2 or 1
  for i = HUD_ID, arenas_amnt do
    arenas[i].y_off = arenas[i].y_off - SLOT_DISTANCE / mov_divisor
  end

  -- se ora ci sono meno slot di quelli massimi..
  if arenas_amnt < MAX_ARENAS_IN_STATUS then

    -- aggiorno la posizione degli slot che venivano prima
    for i = 1, HUD_ID -1 do
      arenas[i].y_off = arenas[i].y_off + SLOT_DISTANCE / 2
    end

    -- e faccio sparire tutti quelli in eccesso (non solo l'ultimo, perché quando
    -- un giocatore esce da una partita potrebbero essere spariti più pannelli
    -- di quelli che vedeva prima di entrare, tipo partite finite nel mentre)
    for _, pl_name in pairs(hub.get_players_in_hub()) do
      for i = arenas_amnt +1, MAX_ARENAS_IN_STATUS do
        local panel = panel_lib.get_panel(pl_name, "hubman_arenastatus_" .. i)
        if panel.visible then
          panel:hide()
        end
      end
    end
  end

  -- aggiorno tutti i pannelli visibili
  for i = 1, arenas_amnt do
    hub.HUD_arstatus_update(i)
  end
end



function hub.HUD_arstatus_update(slot_ID, skip_mgarena, skip_pos, skip_status, skip_amount)
  if slot_ID > MAX_ARENAS_IN_STATUS then return end

  local slot = arenas[slot_ID]
  local elem = {}
  local subtxt_elems = {}
  local icon = nil
  local status = nil

  if not skip_mgarena then
    icon = { icon = { text = slot.icon }}
    subtxt_elems.arena_name = { text = trim_name(slot.name) }
  end

  if not skip_pos then
    elem.offset = { x = 0, y = slot.y_off }
  end

  local _, arena = arena_lib.get_arena_by_name(slot.mod, slot.name)
  local mg = arena_lib.mods[slot.mod]

  if not skip_status then
    local bg_img = ""

    if not arena.in_game and not arena.in_queue then
      bg_img = "hub_arenastatus_waiting.png"
    elseif not arena.in_loading and not arena.in_celebration and
       arena.players_amount < arena.max_players * #arena.teams and
       (arena.in_queue or (arena.in_game and mg.join_while_in_progress)) then
      bg_img = "hub_arenastatus_join.png"
    else
      bg_img = "hub_arenastatus_nojoin.png"
    end

    slot.bg = bg_img
    elem.bg = bg_img
  end

  if not skip_amount then
    local amount = arena.players_amount .. "/" .. arena.max_players * #arena.teams

    slot.amount = amount
    subtxt_elems.p_amount = { text = amount }
  end

  -- se non c'è nessun elemento/sottoelemento da aggiornare, faccio sparire
  if not next(elem) then
    elem = nil
  end

  if not next(subtxt_elems) then
    subtxt_elems = nil
  end

  for _, pl_name in pairs(hub.get_players_in_hub()) do
    local panel = panel_lib.get_panel(pl_name, "hubman_arenastatus_" .. slot_ID)
    panel:update(elem, icon, subtxt_elems)

    -- se non era visibile e l'opzione per non vederlo non era abilitata, mostralo
    if not panel:is_visible() and not hidden_panels[pl_name] then
      panel:show()
    end
  end
end



function hub.HUD_arstatus_show(p_name)
  if not hidden_panels[p_name] then return end

  for i = 1, #arenas do
    if i > MAX_ARENAS_IN_STATUS then break end

    local panel = panel_lib.get_panel(p_name, "hubman_arenastatus_" .. i)
    panel:show()
  end

  hidden_panels[p_name] = nil
end



function hub.HUD_arstatus_hide(p_name)
  if hidden_panels[p_name] then return end

  for i = 1, MAX_ARENAS_IN_STATUS do
    local panel = panel_lib.get_panel(p_name, "hubman_arenastatus_" .. i)
    panel:hide()
  end

  hidden_panels[p_name] = true
end



function hub.get_arenastatus_slot(mod, arena_name)
  for i = 1, #arenas do
    if arenas[i].name == arena_name and arenas[i].mod == mod then
      return i
    end
  end
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function trim_name(name)
  if name:len() > 15 then
    return name:sub(1, 13) .. "..."
  else
    return name
  end
end
