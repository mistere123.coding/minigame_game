local S = minetest.get_translator("hub")



minetest.register_tool("hub_core:settings", {

  description = S("Settings"),
  inventory_image = "hub_settings.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = "2"},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    minetest.chat_send_player(user:get_player_name(), "Coming soon")
  end

})
