local version = "0.1.0-dev"
local srcpath = minetest.get_modpath("hub_core") .. "/src/"
hub = {}
hub.settings = {}



dofile(srcpath .. "/_load.lua")
dofile(minetest.get_worldpath() .. "/hub/SETTINGS.lua")

dofile(srcpath .. "/api.lua")
dofile(srcpath .. "/lock_inventory.lua")
dofile(srcpath .. "/celestial_vault.lua")
dofile(srcpath .. "/chat.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/items.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/privs.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/deps/arena_lib.lua")
dofile(srcpath .. "/HUD/hud_arenastatus.lua")


minetest.log("action", "[HUB] Mod initialised, running version " .. version)
