# playerlist

Shows a list of all online players (similar to Minecraft's tab-list) when a player is pressing E whilst in the hub.

Originated from Fleckenstein's [playerlist](https://content.minetest.net/packages/Fleckenstein/playerlist/) mod. I don't really consider it a fork, being the ping formula the only thing left from the original mod.
