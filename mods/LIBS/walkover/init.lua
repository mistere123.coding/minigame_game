-- register extra flavours of a base nodedef
local timer = 0
core.register_globalstep(function(dtime)
	timer = timer + dtime;
	if timer >= 1 then
		for _,player in pairs(core.get_connected_players()) do
            local loc = vector.add(player:get_pos(),{x=0,y=-1,z=0})
            if loc ~= nil then
               
                local nodeiamon = core.get_node(loc)
                if nodeiamon ~= nil then
                    local def = core.registered_nodes[nodeiamon.name]
                    if def ~= nil and def.on_walk_over ~= nil then
                        def.on_walk_over(loc, nodeiamon, player)
                    end
                end   
            end
        end
	 
		timer = 0
	end
end)