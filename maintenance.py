import subprocess
import os
import re
import shutil
import stat
from urllib.parse import urlparse

def menu(options, title="Select an option"):
    print(title)
    for i, option in enumerate(options, 1):
        print(f"{i}. {option}")

    choice = 0
    while not 1 <= choice <= len(options):
        try:
            choice = int(input("Enter your choice: "))
            if not 1 <= choice <= len(options):
                print("Invalid choice. Please try again.")
        except ValueError:
            print("Invalid input. Please enter a number.")

    return options[choice - 1]

def onerror(func, path, exc_info):
    """
    Error handler for ``shutil.rmtree``.

    If the error is due to an access error (read-only file)
    it attempts to add write permission and then retries.

    If the error is for another reason it re-raises the error.
    """
    if not os.access(path, os.W_OK):
        os.chmod(path, stat.S_IWUSR)
        func(path)
    else:
        raise

def run_command(command):
    """Run a shell command and return the output."""
    try:
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        return result.stdout.strip()
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {e.stderr.strip()}")
        exit(1)

def add_module():
    """Add a submodule to the repository."""
    target_dir = menu(["GAMES","HELPERS","LIBS"], "Where do you want to add it? Select one:")
    if target_dir != "GAMES" and target_dir != "HELPERS" and target_dir != "LIBS":
        print("Invalid, try again.")
        return
    repo_url = input("Enter URL: ")
    pattern = r"(https?|git|ssh|rsync)\:\/\/[A-Za-z0-9\.\-\_]+\.(com|org|net|mil|edu|COM|ORG|NET|MIL|EDU)\/[A-Za-z0-9\.\-\_]+\/[A-Za-z0-9\.\-\_]+(\.git)?"
    if not(re.match(pattern, repo_url)):
        print(f"{repo_url} is not a valid git repository URL.")
        return
    path = urlparse(repo_url).path
    repo_name = path.split("/")[-1].split(".")[0]
    target_path = f"./mods/{target_dir}/{repo_name}"
    run_command(["git", "submodule", "add", repo_url, target_path])
    print(f"Submodule added at {target_path}")

def list_modules():
    """List all submodules."""
    output = run_command(["git", "submodule"])
    # Extract just the submodule names
    submodules = re.findall(r"\s+(\S+)\s+\(", output)
    for submodule in submodules:
        print(submodule.split('/')[-1])

def delete_module():
    """Remove a submodule."""
    submodule_name = input("Enter the name of the submodule: ")
    # Finding the correct path
    output = run_command(["git", "submodule"])
    match = re.search(rf"\s+(mods/[^/]+/{submodule_name})\s+\(", output)
    if match:
        submodule_path = match.group(1)
        # Extract the relevant parts of the path for the git config
        path_parts = submodule_path.split('/')
        path_parts.insert(0,"mods")
        submodule_config_section = 'submodule.' + '/'.join(path_parts[1:])
        print("Removing "+submodule_path)
        # run_command(["git", "config", "--file=.gitmodules", "--remove_section", "submodule"])

        run_command(["git", "submodule", "deinit", "-f", "--", submodule_path])
        # run_command(["git", "config", "--remove-section", submodule_config_section])
        run_command(["git", "rm", "--cached", submodule_path])
        shutil.rmtree(".git/modules/" + submodule_path,onerror=onerror)
        shutil.rmtree(submodule_path,onerror=onerror)

        print(f"Successfully removed submodule {submodule_name} from {submodule_path}")
    else:
        print(f"Submodule {submodule_name} not found.")



def update_submodules():
    """Update all submodules."""
    run_command(["git", "submodule", "update", "--recursive","--remote", "--merge"])
    run_command(["git", "commit", "-am", "Updated submodules"])
    print("Submodules updated.")

def main():
    while True:

        choice = menu(["Add Module","List Modules","Delete Module","Update Submodules","Exit"], "Git Submodule Helper")

        if choice == "Add Module":
            add_module()
        elif choice == "List Modules":
            list_modules()
        elif choice == "Delete Module":
            delete_module()
        elif choice == "Update Submodules":
            update_submodules()
        elif choice == "Exit":
            break
        else:
            print("Invalid choice. Please try again.")

if __name__ == "__main__":
    main()
